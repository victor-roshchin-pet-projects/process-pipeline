#pragma once
#include "DataVar.h"

using namespace std;



class cPipe
{
	int pipeMaterial { 0 };				// ��� ������ �������� ����� ��. �������
	float d{ 0.00 };					// �������� ������� �����
	float Pres{ 0.00 };					// ��������� ��������
	int serLife{ 0 };					// ���� ������ ������������

	string steelGrade { "" };			// ����� ����� ��� ����������
	
	int tempRes { 0 };					// ����������� �������� ���������� �������������
	int yStr{ 0 };						// ����������� �������� ������� ���������
	float permPres{ 0.00 };				// ����������� ����������� ����������
	float C{ 0.00 };					// ����� ��������
	
	float Sr{ 0.00 };					// ��������� ������� ������
	float S{ 0.00 };					// ������� ������

public:

	// ������� ��� ����� ������������� �������� ������
	int pipeMaterialF();
	float dF();
	float PresF();
	int serLifeF();

	// ������� ��� ������ ����� ����� � � �������������
	void steelGraidF();
	int tempResF();
	int yStrF();
	
	// ������� ������� ��������� ������ ��� ������������� ������������ ����� ���� �������� � �����
	void print();
	void check();

	// ������������� �������
	float permPresF(); // ������ ������������ ������������ ����������
	float SrF(); // ������� ��������� ������� ������
	float CF();


	// �������� ������ (�������� � ���� ��� �������������� �������)
	void SF();
	void printRep();

cPipe() {};

//cPipe(int* pipeMaterial, float* d, float* Pres, int* serLife)
//{
//	this -> pipeMaterial = *pipeMaterial;
//	this-> d = *d;
//	this-> Pres = *Pres;
//	this-> serLife = *serLife;
//};



friend void operator<< (ostream& s, cPipe& x)
{
	s << "����� �����: " << x.steelGrade << "\n";
	s << "�������� ������� ������������: " << x.d << " ��\n";
	s << "��������� ��������: " << x.Pres << " ���\n";
	s << "��������� ���� ������ ������������: " << x.serLife << " ���\n";
	s << "��������: " << x.C << " ��\n";
	s << "��������� ������� ������: " << x.S << " ��\n";
}

friend void operator>> (istream& s, cPipe& x)
{
	// ����� �����
	cout << "�������� ����� ����� ������������:\n" << "1 - 09�2C, 2 - 12�18�10�\n";
	
	s >> x.pipeMaterial;
	while (!x.pipeMaterial || x.pipeMaterial >= 3 || x.pipeMaterial <= 0 || cin.peek() == ',') {
		cout << "error\n";
		cin.clear();
		cin.ignore(100, '\n');
		cin >> x.pipeMaterial;
	}

	// �������
	cout << "������� ������� �����, ��\n";
	s >> x.d;

	float fraction = .0;

	while (true)
	{
		cout << "���� �������� �����\n";
		if (cin.peek() == ',')
		{
			cin.ignore();
			cin >> fraction;

			if (!fraction)
				cout << "���� ����� ��������\n";
				cin.clear();
				cin.ignore(100, '\n');
				cout << "������� �����\n";
				cin >> x.d;

			while (int(fraction))  // ���� ������� ����� �� ������ 1
				fraction /= 10;
			x.d += fraction;

		}
		else if (!x.d)
		{
			cout << "���� ����� ��������\n ������� �����";
			cin.clear();
			cin.ignore(100, '\n');
			cin >> x.d;
		}
		
		if (fraction)
			cout << "����� �� ���� �������� �����\n";
			break;
	}

	//if (cin.peek() == ',') { //���� ��������� ������ �������
	//	cin.ignore();        //���������� �������
	//	cin >> fraction;     // ������ ������� �����
	//	if (!fraction)
	//	{
	//		cout << "error\n";
	//		cin.clear();
	//		cin.ignore(100, '\n');
	//		cin >> x.pipeMaterial;
	//	}
	//	if (!fraction)

	//}

	

	/*while (!x.d) {
		cout << "error\n";
		cin.clear();
		cin.ignore(100, '\n');
		cin >> x.d;
	}*/

	// ��������
	cout << "������� ��������� ��������, ���\n";
	s >> x.Pres;

	if (cin.peek() == ',') { //���� ��������� ������ �������
		cin.ignore();        //���������� �������
		cin >> fraction;     // ������ ������� �����        
	}
	while (int(fraction))  // ���� ������� ����� �� ������ 1
		fraction /= 10;
	x.Pres += fraction;

	while (!x.Pres) {
		cout << "error\n";
		cin.clear();
		cin.ignore(100, '\n');
		cin >> x.Pres;
	}

	// ���� ������
	cout << "������� ���� ������ ������������, ���\n";
	s >> x.serLife;
	while (!x.serLife) {
		cout << "error\n";
		cin.clear();
		cin.ignore(100, '\n');
		cin >> x.serLife;
	}
}
};